import avatar from '../imgs/avatar-svgrepo-com.svg'


export default function Profile () {
    return <>
        <img src={avatar} alt='avatar' />
        <h2> Some Person </h2>
    </>
}
